package com.epsi.poe.basis;
import java.util.*;

import com.epsi.poe.tpnote.Client;
import com.epsi.poe.tpnote.Film;
import com.epsi.poe.tpnote.Menu;


public class Main {

    public static HashMap<String, Film> listeFilms = new HashMap<>();
    public static HashMap<String, Client> listeCLients = new HashMap<>();
    public static List<String> support = new ArrayList<>();


    private static final String lK7 = "K7";
    private static final String lCle = "USB";
    private static final String lBlu = "BLUERAY";
    private static final String lDvd = "DVD";



    public static void main(String[] args) {

        listeFilms.put("The Shinning", new Film("The Shinning","SpongeBob",1980,"Patrick",lK7,false));
        listeFilms.put("300", new Film("300","Gladitorus",1998,"Johnny Joe",lBlu, false));
        listeFilms.put("Les misérables", new Film("Les misérables","Will Smith",2008,"Louis Dupont",lCle, false));
        listeFilms.put("Bogota", new Film("Bogota","John Doe",2020,"The Rock",lDvd, false));

        listeCLients.put("Norbert", new Client("Norbert","Léo", "NorbertLeo@hotmail.fr" ));
        listeCLients.put("Julien", new Client("Julien","Monfeuille", "jmonfeuille@gmail.com" ));
        listeCLients.put("Lucien", new Client("Lucien","Picard", "picardl@epsi.fr" ));

        Menu.start();




    }









}


