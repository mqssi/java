package com.epsi.poe.tpnote;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Film {


    public String nom;
    public String acteurPrincipal;
    public int anneeSortie;
    public String realisateur;
    public String support;
    public boolean loue;


/*
List<String> support = new ArrayList<String>();
    private static final String lK7 = "K7";
    private static final String lCle = "clé USB";
    private static final String lBlu = "Blue-ray";
    private static final String lDvd = "DVD";

        support.add(lK7);
        support.add("clé USB");
        support.add("Blue-ray");
        support.add("DVD");
*/
    public Film (){

        this.nom="null";
        this.acteurPrincipal="null";
        this.anneeSortie=0;
        this.realisateur="null";
        this.loue = loue;

    }

    public Film (String _nom,String _acteurPrincipal, int _anneeSortie, String _realisateur, String _support, Boolean loue){

      this.nom = _nom;
      this.acteurPrincipal = _acteurPrincipal;
      this.anneeSortie = _anneeSortie;
      this.realisateur = _realisateur;
      this.support = _support;
      this.loue = loue;
    }


    public String affichageComplet () {

        return "Le nom du film est " + this.nom + " de l'acteur principal " + this.acteurPrincipal +
                " l'année est " + this.anneeSortie + " le réalisateur " + this.realisateur + " Et comme support " + this.support + " ce film est-il louée? " + this.loue;

    }

    public void modifLoue (boolean loue){

        this.loue = loue;

    }

    public String affichageNom () {

        return this.nom;

    }

    public String affichageActeur () {

        return this.acteurPrincipal;

    }

    public int affichageAnnee () {

        return this.anneeSortie;

    }

    public String affichageRealisateur () {

        return this.realisateur;

    }


    public String afficheSupport() {

        return support;

    }




}
