package com.epsi.poe.tpnote;
import com.epsi.poe.basis.Main;

import java.util.Locale;
import java.util.Map;
import java.util.Scanner;


public class Menu {

        public static void start () {
                Scanner sc = new Scanner(System.in);
                int choix;



                do{


                        System.out.println("Voici le menu, que voulez-vous faire?");
                        System.out.println("1. Lister les films");
                        System.out.println("2. Ajouter un film");
                        System.out.println("3. Afficher la liste de film disponibles et loués");
                        System.out.println("4. Afficher la liste de film par support");
                        System.out.println("5. Supprimer un film par le nom");
                        System.out.println("6. Rechercher un film par le nom");
                        System.out.println("7. Afficher tous les clients");
                        System.out.println("8. Ajouter un client");
                        System.out.println("9. Louer un film pour un client");
                        System.out.println("10. Afficher les films loués d'un client");
                        System.out.println("0. Quitter");

                        choix = sc.nextInt();

                        switch (choix){

                                case 1:
                                        afficherFilms();
                                        break;
                                case 2:
                                        ajouterFilm();
                                        break;
                                case 3:
                                        afficherFilmDispo();
                                        break;
                                case 4:
                                        afficherParSupport();
                                        break;
                                case 5:
                                        deleteFilm();
                                        break;

                                case 6:
                                        chercherFilm();
                                        break;


                                case 7:
                                        afficherClients();
                                        break;

                                case 8:
                                        ajouterClients();
                                        break;

                                case 9:
                                        loueFilmClients();
                                        break;
                                case 10:
                                        voirFilmsClient();
                                        break;
                                case 0:
                                        break;
                                default :
                                        System.out.println("Entrée invalide");
                                        break;

                        }



                } while ( choix != 0);
                 sc.close();
                System.out.println("Au revoir");

        }

        public  static void  afficherFilms()   {
                System.out.println("Voici la liste des films");
                for(Map.Entry listeFilm : Main.listeFilms.entrySet()){
                        System.out.println(listeFilm.getKey());
                }

        }

        public  static void  afficherClients()   {
                System.out.println("Voici la liste des clients");
                for(Map.Entry listeClient : Main.listeCLients.entrySet()){
                        System.out.println(listeClient.getKey());


                }

        }


        public  static void afficherParSupport() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez le support voulu: K7, USB, BlUERAY, DVD.");
        String support = sc.nextLine();

        for (Map.Entry listeFilm : Main.listeFilms.entrySet()) {
                        Film f = (Film) listeFilm.getValue();

                        switch (support.toUpperCase()) {
                                case "K7":
                                        if (f.afficheSupport().equals("K7")) {
                                                System.out.println(f.affichageNom());
                                        }
                                        break;

                                case "USB":
                                        if (f.afficheSupport().equals("USB")) {
                                                System.out.println(f.affichageNom());
                                        }
                                        break;
                                case "BLUERAY":
                                        if (f.afficheSupport().equals("BLUERAY")) {
                                                System.out.println(f.affichageNom());
                                        }
                                        break;
                                case "DVD":
                                        if (f.afficheSupport().equals("DVD")) {
                                                System.out.println(f.affichageNom());
                                        }
                                        break;
                                default:
                                        System.out.println("Entrée invalide");
                                        break;

                                }
                        }


                        /*
                        if (support.equalsIgnoreCase("K7")) {
                                if (f.afficheSupport().equals("K7")) {
                                        System.out.println(f.affichageNom());
                                }
                        } else if (support.equalsIgnoreCase("clé USB")) {
                                if (f.afficheSupport().equals("clé USB")) {
                                        System.out.println(f.affichageNom());
                                }
                        } else if (support.equalsIgnoreCase("Blue-ray")) {
                                if (f.afficheSupport().equals("Blue-ray")) {
                                        System.out.println(f.affichageNom());
                                }
                        } else if (support.equalsIgnoreCase("DVD")) {
                                if (f.afficheSupport().equals("DVD")) {
                                        System.out.println(f.affichageNom());
                                }
                        }


                        */


        }


        public static void ajouterFilm() {

                Scanner sc = new Scanner(System.in);


                System.out.println("Ajouter le nom du film");
                String  choixNomFilm = sc.nextLine();

                System.out.println("Ajouter l'acteur principal du film");
                String choixActeurFilm = sc.nextLine();

                System.out.println("Ajouter le réalisateur du film");
                String choixRealisateurFilm = sc.nextLine();

                System.out.println("Ajouter le support du film");
                String choixSupportFilm = sc.nextLine();


                System.out.println("Ajouter l'année du film");
                int choixAnneeFilm = sc.nextInt();



                Film monFilmCree = new Film(choixNomFilm,choixActeurFilm,choixAnneeFilm,choixRealisateurFilm,choixSupportFilm,false);
                Main.listeFilms.put(choixNomFilm, monFilmCree);
                System.out.println("Le film " + choixNomFilm + " a été ajouté");


                afficherFilms();


        }



        public static void ajouterClients() {

                Scanner sc = new Scanner(System.in);

                System.out.println("Ajouter le nom du client");
                String  choixNomCli = sc.nextLine();

                System.out.println("Ajouter le prénom du client");
                String choixPrenomCli = sc.nextLine();

                System.out.println("Ajouter le mail du client");
                String choixMailCli = sc.nextLine();

                Client monClientCree = new Client(choixNomCli,choixPrenomCli,choixMailCli);
                Main.listeCLients.put(choixNomCli, monClientCree);
                System.out.println("Le client " + choixNomCli + " " + choixPrenomCli  + " a été ajouté");


                afficherClients();


        }

        public static void afficherFilmDispo(){

                System.out.println("Voici la liste des films disponibles");
                for(Map.Entry listeFilm : Main.listeFilms.entrySet()){
                        Film filmLouable = (Film) listeFilm.getValue();
                        if(!filmLouable.loue) {
                        System.out.println(listeFilm.getKey());
                        }
                }

                System.out.println("Voici la liste des films loués ");
                for(Map.Entry listeFilm : Main.listeFilms.entrySet()){
                        Film filmNonLouable = (Film) listeFilm.getValue();
                        if(filmNonLouable.loue) {
                                System.out.println(listeFilm.getKey());
                        }
                }


        }



        public static void deleteFilm(){



                Scanner sc = new Scanner(System.in);


                System.out.println("Ajouter le nom du film à supprimer");
                String  filmADelete = sc.nextLine();


                if (Main.listeFilms.containsKey(filmADelete) ){

                        System.out.println("Supression du film " + filmADelete);
                        Main.listeFilms.remove(filmADelete);

                }
                else {
                        System.out.println("Le film " + filmADelete + " n'existe pas");

                }


                afficherFilms();



        }

        public static void chercherFilm() {

                afficherFilms();
                System.out.println("Quel film voulez vous inspecter?");
                Scanner sc = new Scanner(System.in);

                String  filmAInspecter = sc.nextLine();

                if (Main.listeFilms.containsKey(filmAInspecter) ){
                        Film infosFilm = Main.listeFilms.get(filmAInspecter);

                        System.out.println(infosFilm.affichageComplet());
                }
                else {
                        System.out.println("Le film " + filmAInspecter + " n'existe pas");
                }


        }


        public static void loueFilmClients(){
                Scanner sc = new Scanner(System.in);
                System.out.println("Veuiller entre le nom du client");
                String nomCli = sc.nextLine();

                        if (Main.listeCLients.containsKey(nomCli)) {

                                System.out.println("Entrez le nom du film");
                                String nomFilmCliLoue = sc.nextLine();


                                                if (Main.listeFilms.containsKey(nomFilmCliLoue)) {

                                                                        if(!Main.listeFilms.get(nomFilmCliLoue).loue){

                                                                                        Main.listeFilms.get(nomFilmCliLoue).modifLoue(true);
                                                                                        Main.listeCLients.get(nomCli).modifLocation().add(Main.listeFilms.get(nomFilmCliLoue));
                                                                                        System.out.println("Le film a été attribué au client");
                                                                        }else {
                                                                                System.out.println("Ce film est déja loué");
                                                                        }


                                                }else {
                                                        System.out.println("Ce film n'existe pas");
                                                }



                        }else{
                                System.out.println("Ce client n'existe pas");

                        }



        }


        public static void voirFilmsClient() {

                Scanner sc = new Scanner(System.in);
                System.out.println("Veuiller entre le nom du client");
                String nomCli = sc.nextLine();


                if (Main.listeCLients.containsKey(nomCli)) {

                        if(Main.listeCLients.get(nomCli).modifLocation().size()== 0){


                                System.out.println("Le client ne loue aucun film");

                        }else {
                                System.out.println(nomCli+" loue : ");
                                for (Film f : Main.listeCLients.get(nomCli).modifLocation()) {
                                        System.out.println(f.affichageNom());
                                }
                        }






                }else{
                        System.out.println("Ce client n'existe pas");

                }




        }









}
