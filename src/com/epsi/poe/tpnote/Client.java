package com.epsi.poe.tpnote;

import java.util.ArrayList;
import java.util.List;

public class Client {

    public String nom;
    public String prenom;
    public String mail;
    private List<Film> filmsLoues = new ArrayList<>();


    public List<Film> modifLocation() {
        return filmsLoues;
    }

    public Client(String _nom, String _prenom, String _mail) {
        this.nom = _nom;
        this.prenom = _prenom;
        this.mail = _mail;
    }


    public String affichageNomClient() {
        return nom;
    }

    public void modifNomClient(String nom) {
        this.nom = nom;
    }

    public String affichagePrenomClient() {
        return prenom;
    }

    public void modifPrenomClient(String prenom) {
        this.prenom = prenom;
    }

    public String affichageMailClient() {
        return mail;
    }

    public void affichageMailClient(String mail) {
        this.mail = mail;
    }



}